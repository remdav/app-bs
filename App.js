import { ThemeProvider } from "emotion-theming"
import Navigation from "./src/navigation"
import { Provider } from 'react-redux'
import theme from "config/theme.json"
import store from "reduxStore"
import React from "react"


export default function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme} >
        <Navigation />
      </ThemeProvider>
    </Provider>
  )
}
