module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            "navigation": './src/navigation',
            "application": './src/application',
            "reduxStore": './src/reduxStore',
            "validation": './src/validation',
            "assets": "./assets",
            "utils": './utils',
            "config": './config',
          },
        },
      ],
    ],
  };
};
