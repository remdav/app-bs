import * as SecureStore from "expo-secure-store"
import { Button } from 'react-native'
import React from 'react'

export default function Logout({ navigation }) {
  async function logout() {
    await SecureStore.deleteItemAsync("token_key")
    await SecureStore.deleteItemAsync("key")

    return navigation.navigate("connection")
  }

  return (
    <Button
      title="Deconnexion"
      onPress={logout}
    />
  )
}
