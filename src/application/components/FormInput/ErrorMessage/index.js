import { View } from "react-native"
import React from "react"

import { Text } from "./components"

export default function ErrorMessage({ message }) {
  return (
    <View>
      <Text>{ message }</Text>
    </View>
  )
}
