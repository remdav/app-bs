import { TextInput as TextInputComponent } from "application/components/Types/styled"
import { View } from "react-native"
import theme from "config/theme.json"
import React from "react"

import { Text } from "./components"

export default function TextInput({ label, formikProps, name, placeholder, ...rest }) {
    return (
        <View>
            <TextInputComponent
                placeholder={placeholder}
                onChangeText={formikProps.handleChange(name)}
                onBlur={formikProps.handleBlur(name)}
                selectionColor={theme.yellow}
                underlineColorAndroid ={theme.yellow}
                placeholderTextColor={theme.white}
                {...rest}
                style={{ color: "white" }}
            />
            <Text>{formikProps.touched[name] && formikProps.errors[name]}</Text>
        </View>
    )
}