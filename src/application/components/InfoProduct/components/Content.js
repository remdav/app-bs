import styled from "@emotion/native"

const Content = styled.View`
  ${props => props.isFlex && "flex: 1;"}
  align-items: center;
`

export default Content