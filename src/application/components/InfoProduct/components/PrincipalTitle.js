import styled from "@emotion/native"

const PrincipalTitle = styled.Text`
  font-size: 50px;
  font-weight: bold; 
  text-decoration: underline;
  color: ${props => props.theme.yellow};
`

export default PrincipalTitle