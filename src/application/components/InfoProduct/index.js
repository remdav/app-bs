import { Image } from "application/components/Types"
import React from "react"

import { Content, PrincipalTitle } from "./components"

export default function InfoProduct({info, children, isFlex = false}) {
    return (
        <Content isFlex={isFlex}>
            <Image source={info.image} header={true} resizeScreen={1.1} />
            <PrincipalTitle>{info.title}</PrincipalTitle>
            {children}
        </Content>
    )
}