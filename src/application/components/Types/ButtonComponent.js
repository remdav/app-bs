import ZO from "react-native-vector-icons/Zocial"
import theme from "config/theme.json"
import { firstLetterUC } from "utils"
import React from "react"

import { Button, ButtonView } from "./styled"
import Text from "./TextComponent"

function ButtonComponent({
  title,
  onPress,
  facebook = false,
  google = false,
  width,
  color = theme.yellow,
  disabled = false,
  variant,
  textColor
}) {
  return (
    <Button
      onPress={onPress}
      facebook={facebook}
      google={google}
      width={width}
      color={color}
      disabled={disabled}
      variant={variant}
    >
      <ButtonView>
        <Text color={textColor}>{firstLetterUC(title)}</Text>
        {facebook && (
          <ZO name="facebook" size={15} />
        )}
        {google && (
          <ZO name="google" size={15} />
        )}
      </ButtonView>
    </Button>
  )
}

export default ButtonComponent