import React from "react"

import { Container as ContainerStyled } from "./styled"

export default function Container({
  children,
  alignItems = "center",
  justifyContent = "center",
  marginBottom,
  marginTop = "50",
  flexDirection,
  flexWrap
}) {
  return (
    <ContainerStyled
      alignItems={alignItems}
      justifyContent={justifyContent}
      marginBottom={marginBottom}
      marginTop={marginTop}
      flexDirection={flexDirection}
      flexWrap={flexWrap}
    >
      {children}
    </ContainerStyled>
  )
}