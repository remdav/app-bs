import config from "config/config.json"
import theme from "config/theme.json"
import React from "react"

import { Image as ImageStyled } from "./styled"

export default function Image({ source, variant, resizeScreen = true, local = false, header = false }) {
  function outlined() {
    if(variant === "outlined") {
      return {
        borderWidth: 5,
        borderColor: theme.yellow,
      }
    }
  }

  return(
    <ImageStyled
        source={local ? source : {uri: `${config.global.images}/${source}`}}
        variant={variant}
        style={outlined}
        resizeScreen={resizeScreen}
        header={header}
    />
  )
} 