import React from "react"
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native"

export default function MyLoader(props) {
    return(
        <ContentLoader 
        speed={0.3}
        width={400}
        height={160}
        viewBox="0 0 400 160"
        backgroundColor="#222222"
        foregroundColor="#f2c500"
        {...props}
      >
        <Rect x="9" y="17" rx="20" ry="20" width="179" height="60" /> 
        <Rect x="10" y="97" rx="20" ry="20" width="177" height="59" /> 
        <Rect x="195" y="17" rx="19" ry="19" width="191" height="60" /> 
        <Rect x="194" y="96" rx="20" ry="20" width="195" height="60" />
      </ContentLoader>
    )
}
