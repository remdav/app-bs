import config from "config/config.json"
import Image from "./Image"
import React from "react"

import {Title, ListStyled, ContentText} from "./components"

export default function Container({item, navigation, to}) {
    function redirect() {
        return navigation.navigate(to, { id: item.id })
    }

    return (
        <ListStyled onPress={redirect}>
            <Image
                source={{
                    uri: `${config.global.images}/${item.image}`
                }}
            />
            <ContentText>
                <Title isTitle={true}>{item.title}</Title>
            </ContentText>
        </ListStyled>
    )
}