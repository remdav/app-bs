import React from "react"

import { Image as ImageStyled } from "./components"

export default function Image({ source }) {
  return (
    <ImageStyled source={source} resizeMode='cover'/>
  )
}