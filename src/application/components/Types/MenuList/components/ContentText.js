import {autoRadius} from "utils/style"
import styled from "@emotion/native"

const borderRadius = autoRadius(true)

const ContentText = styled.View`
    ${borderRadius};
    backgroundColor: ${props => props.theme.yellow};
    flex-basis: 50;
    justify-content: center;
    margin-left: -1px;
`

export default ContentText