import styled from "@emotion/native"

const FlatListStyled = styled.FlatList`
  marginLeft: 20px;
  marginRight: 20px;
  marginBottom: 40px;
`

export default FlatListStyled