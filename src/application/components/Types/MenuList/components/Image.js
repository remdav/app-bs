import styled from "@emotion/native"

const Image = styled.Image`
    flex:1;
    border-radius: 35px 35px 40px 40px;
`

export default Image