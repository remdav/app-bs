import { autoRadius } from "utils/style"
import styled from "@emotion/native"

const borderRadius = autoRadius()

const ListStyled = styled.TouchableOpacity`
  flex: 1;
  ${borderRadius}
  width: 200px;
  height: 200px;
  marginBottom: 10px;
  marginLeft: 10px;
  border-width: 4px;
  border-style: solid;
  border-color: ${props => props.theme.yellow};
`

export default ListStyled