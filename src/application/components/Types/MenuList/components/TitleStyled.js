import styled from "@emotion/native"

const Text = styled.Text`
  color: ${props => props.theme.white};
  ${props => props.isTitle ? "text-align: center; fontSize: 15px;" : "left: 70%; fontSize: 15px;"}
`

export default Text