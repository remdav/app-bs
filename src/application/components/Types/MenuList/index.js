import React from "react"

import { FlatList as FlatListStyled } from "./components"
import Container from "./Container"

export default function FlatList({ data , numColumns, navigation, to}) {
  function getItemByData({ item }) {
    return (
      <Container item={item} navigation={navigation} to={to} idItem={getIdByItem}/>
    )
  }

  function getIdByItem(item) {
    return item.id
  }

  return (
    <FlatListStyled
      data={data}
      numColumns={numColumns}
      renderItem={getItemByData}
      keyExtractor={getIdByItem}
    >
    </FlatListStyled>
  )
}