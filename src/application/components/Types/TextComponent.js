import styled from "@emotion/native"

const TextComponent = styled.Text`
  color: ${props => props.color};
`

export default TextComponent