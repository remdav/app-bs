import Icon from "react-native-vector-icons/AntDesign"
import theme from "config/theme.json"
import React from "react"

import { TextInput as TextInputComponent, TextInputView } from "./styled"

export default function TextInput({ onBlur, onChangeText, placeholder, isHeader }) {
    return (
        <TextInputView>
            <TextInputComponent
                placeholder={placeholder}
                onChangeText={onChangeText}
                onBlur={onBlur}
                selectionColor={theme.yellow}
                underlineColorAndroid ={theme.yellow}
                placeholderTextColor={theme.white}
                style={{ color: "white" }}
                isHeader={isHeader}
            />
            <Icon name="search1" size={25} style={{color: "white",marginTop: 15}}/>
        </TextInputView>
    )
}