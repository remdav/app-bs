import styled from "@emotion/native"

const ViewComponent = styled.View`
  flex: 1;
  background-color: ${props => props.theme.black};
  justify-content: center;
`

export default ViewComponent