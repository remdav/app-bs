import styled from "@emotion/native"

const ButtonStyled = styled.TouchableOpacity`
  border-radius: 80px;
  color: white;	
  padding: 15px 25px;
  ${props => {
    if (props.variant === "outlined") {
      return `
       border-width: 4px;
       border-style: solid;
       border-color: ${props.theme.yellow};
      `
    } else {
      return `
        background-color: ${
          props.facebook ? "#4C69BA"
          : props.google ? "#4285F4"
          : props.disabled ? props.theme.disabledYellow
          : props.theme.yellow
      };
      `
    }
  }}
  margin-top: 20px;
  width: ${props => props.width}%;
`

export default ButtonStyled