import styled from "@emotion/native"

const ButtonView = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

export default ButtonView