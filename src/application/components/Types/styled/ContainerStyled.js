import styled from "@emotion/native"

const ContainerStyled = styled.View`
  flex: 1;
  flex-direction: ${props => props.flexDirection};
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  margin-bottom: ${props => props.marginBottom}px;
  margin-top: ${props => props.marginTop}px;
  flex-wrap: ${props => props.flexWrap};
`
export default ContainerStyled