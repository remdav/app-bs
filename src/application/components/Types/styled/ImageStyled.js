import styled from "@emotion/native"
import { sizeScreen } from "utils"

const sizeImage = sizeScreen()

const ImageStyled = styled.Image`
height: 300px;
  ${props => {
    if (props.variant === "outlined") {
      return "borderRadius: 40px;"
    }
    
    if(props.resizeScreen) {
      if (props.header) {
        return `
        width: ${sizeScreen(props.resizeScreen)}px;
      `
      } else {
        return `
        width: ${sizeImage}px;
        height: ${sizeImage}px;
      `
      }
    }
    
  }}
`

export default ImageStyled