import styled from "@emotion/native"

const TextInputComponent = styled.TextInput`
  height: 40px;
  padding-left: 6px;
  width: 200px;
  ${props => {
      if(props.isHeader) {
          return `
            margin-top: 50px;
            margin-bottom: 30px;
            width: 80%;
            align-self: center;
          `
      }
    }}
`

export default TextInputComponent