import styled from "@emotion/native"

const TextInputView = styled.View`
  display: flex;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
`

export default TextInputView