import styled from "@emotion/native"

const ViewStyled = styled.View`
  flex: 1;
  background-color: ${props => props.theme.black};
  justify-content: center;
`

export default ViewStyled