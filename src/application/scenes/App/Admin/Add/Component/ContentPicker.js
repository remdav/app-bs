import styled from "@emotion/native"

const ContentPicker = styled.View`
  border-width: 0px 0px 1px 0px;
  border-style: dotted;
  border-color: ${props => props.theme.yellow};
  margin-bottom: 20px;
`

export default ContentPicker