import { View, Button, Container } from "application/components/Types"
import { TextInput } from "application/components/FormInput"
import { validationAnimation } from "validation/animation"
import { post } from "utils/request"
import { Formik } from "formik"
import React from "react"


export default function FormAnimation({ uri, navigation }) {
  function getFormatDateEn(date) {
    const [day, mounth, year] = date.split("/")
    return new Date(`${mounth}/${day}/${year}`).toISOString().slice(0, 19).replace('T', ' ')
  }

  return (
    <Formik
      initialValues={{
        title: "",
        theme: "",
        description: "",
        speaker: "",
        startAt: "",
        endAt: "",
        image: "",
      }}
      validationSchema={validationAnimation}
      onSubmit={async values => {
        const newInfo = {
          ...values,
          startAt: getFormatDateEn(values.startAt),
          endAt: getFormatDateEn(values.endAt)
        }

        try {
          await post(`/${uri}`, newInfo)
        } catch (e) {
          console.log("error", e)
          return
        }

        return navigation.navigate("administration")
      }}
    >
      {formikProps => (
        <View>
          <Container>
            <TextInput
              placeholder="Titre"
              name="title"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Theme de la soirée"
              name="theme"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Description"
              name="description"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Intervenant"
              name="speaker"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Date de début (forme: jj/mm/aaaa)"
              name="startAt"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Date de fin (forme: jj/mm/aaaa)"
              name="endAt"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Nom de l'image"
              name="image"
              formikProps={formikProps}
            />
            <Button
              title="Ajouter"
              onPress={formikProps.handleSubmit}
            />
          </Container>
        </View>
      )}
    </Formik>
  )
}
