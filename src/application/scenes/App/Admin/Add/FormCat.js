import { View, Button, Container } from "application/components/Types"
import { TextInput } from "application/components/FormInput"
import { validationCategory } from "validation/category"
import { post } from "utils/request"
import { Formik } from "formik"
import React from "react"


export default function FormCat({ uri, navigation }) {
  return (
    <Formik
      initialValues={{
        title: "",
        image: "",
      }}
      validationSchema={validationCategory}
      onSubmit={async values => {
        try {
          await post(`/${uri}`, values)
        } catch (e) {
          console.log("error", e)
          return
        }

        return navigation.navigate("administration")
      }}
    >
      {formikProps => (
        <View>
          <Container>
            <TextInput
              placeholder="Titre"
              name="title"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Nom de l'image"
              name="image"
              formikProps={formikProps}
            />
            <Button
              title="Ajouter"
              onPress={formikProps.handleSubmit}
            />
          </Container>
        </View>
      )}
    </Formik>
  )
}
