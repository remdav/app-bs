import { View, Button, Container } from "application/components/Types"
import { TextInput } from "application/components/FormInput"
import { validationMeal } from "validation/meal"
import { post, get } from "utils/request"
import { Formik } from "formik"
import React from "react"

import Picker from "./Picker"


export default function FormMeal({ uri, navigation }) {
  const [categories, setCategories] = React.useState([])

  async function getCategories() {
    let all

    try {
      all = await get(`/categories`)
    } catch (e) {
      console.log("error", e)
      return
    }

    setCategories(all["hydra:member"])
  }

  React.useEffect(() => {
    getCategories()
  }, [])

  return (
    <Formik
      initialValues={{
        categories: "1",
        title: "",
        description: "",
        stock: "",
        price: "",
        image: "",
      }}
      onSubmit={async values => {
        const data = {
          ...values,
          price: parseInt(values.price),
          stock: parseInt(values.stock),
          categories: `/api/categories/${values.categories}`
        }
        
        try {
          await post(`/meals`, data)
        } catch (e) {
          console.log("error", e)
        }

        return navigation.navigate("administration")
      }}
    >
      {(formikProps) => (
        <View>
          <Container>
            <Picker
              items={categories}
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Titre"
              name="title"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Description"
              name="description"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Stock"
              name="stock"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="prix"
              name="price"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Nom de l'image"
              name="image"
              formikProps={formikProps}
            />
            <Button
              title="Ajouter"
              onPress={formikProps.handleSubmit}
            />
          </Container>
        </View>
      )}
    </Formik>
  )
}
