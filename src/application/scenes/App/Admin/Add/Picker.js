import { Picker as PickerComponent } from "react-native"
import React from "react"

import { ContentPicker } from "./Component"

export default function Picker({items, formikProps}) {
  async function changeState(value) {
    await formikProps.setFieldValue("categories", value)
  }

  return(
    <>
      {items.length > 0 && (
        <ContentPicker>
            <PickerComponent
              name="categories"
              placeholder="Catégorie"
              style={{
                height: 40,
                paddingLeft: 6,
                width: 200,
                color: "white",
              }}
              selectedValue={formikProps.values.categories}
              onValueChange={changeState}
              >
              {items.map(category => 
                <PickerComponent.Item key={category.id} label={category.title} value={category.id} />
              )}
            </PickerComponent>
        </ContentPicker>
      )}
    </>
  )
}
