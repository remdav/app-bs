import {View, Container, Button} from "application/components/Types"
import React from "react"

import FormAnimation from "./FormAnimation"
import FormMeal from "./FormMeal"
import FormCat from "./FormCat"

export default function Add({navigation}) {
  const type = navigation.getParam("type")

  return (
    <View>
      <Container>
        {type === "meals" ? 
          <FormMeal 
            uri={type} 
            navigation={navigation}
          /> : type === "categories" ? 
          <FormCat 
            uri={type} 
            navigation={navigation}
          /> :
          <FormAnimation 
            uri={type} 
            navigation={navigation}
          />
          }
      </Container>
    </View>
  )
}