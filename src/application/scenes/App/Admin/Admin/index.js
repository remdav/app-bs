import {View, Container, Button} from "application/components/Types"
import React from "react"

export default function Admin({navigation}) {
  function redirectToCat() {
    navigation.navigate("infoAdmin", {
      type: "categories"
    })
  }

  function redirectToAnim() {
    navigation.navigate("infoAdmin", {
      type: "animations"
    })
  }

  function redirectToMenu() {
    navigation.navigate("infoAdmin", {
      type: "meals"
    })
  }

  return (
    <View>
      <Container>
        <Button
          title="Catégorie de menu"
          onPress={redirectToCat}
        />
      <Button
          title="Animation"
          onPress={redirectToAnim}
        />
        <Button
          title="Menu"
          onPress={redirectToMenu}
        />
      </Container>
    </View>
  )
}