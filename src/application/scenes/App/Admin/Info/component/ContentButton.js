import styled from "@emotion/native"

const ContentButton = styled.View`
  align-items: center;
  margin-bottom: 40px;
`

export default ContentButton