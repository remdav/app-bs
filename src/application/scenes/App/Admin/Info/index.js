import { View, MenuList, Button, Loader, Container } from "application/components/Types"
import { get } from "utils/request"
import React from "react"

import {ContentButton} from "./component"

export default function Info({ navigation }) {
  const[infos, setInfos] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  const type = navigation.getParam("type")

  async function getInfos() {
    let featuresMeals
    setLoading(true)

    try {
      featuresMeals = await get(`/${type}`)
    } catch (e) {
      console.log("error", e)
      return
    }

    setLoading(false)
    setInfos(featuresMeals["hydra:member"])
  }

  function addNavigation() {
    navigation.navigate("add", {
      type
    })
  }

  React.useEffect(() => {
    getInfos()
  }, [])

  return (
    <View>
      {loading ? (
        <Container flexDirection="row" alignItems="flex-start" flexWrap="wrap" marginTop="40">
          <Loader />
        </Container>
      ) : (
        <>
          <ContentButton>
            <Button
              title="Ajouter"
              width="80"
              onPress={addNavigation}
            />
          </ContentButton>
          {!!infos && (
              <MenuList
                data={infos}
                numColumns={2}
                navigation={navigation}
                to="mealInfo"
              />
          )
          }
      </>
      )
    }
    </View >
  )
}