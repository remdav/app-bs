import {View, Container, MenuList, Loader} from "application/components/Types"
import {get} from "utils/request"
import React from "react"

export default function Animation({ navigation }) {
  const [animations, setAnimation] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  
  async function getAll() {
    let allAnimation
    setLoading(true)

    try {
      allAnimation = await get("/animations")
    } catch (e) {
      console.log(e)
    }

    setLoading(false)
    setAnimation(allAnimation["hydra:member"])
  }

  React.useEffect(() => {
    getAll()
  }, [])

  return(
    <View>
      {loading ? (
        <Container flexDirection="row" alignItems="flex-start" flexWrap="wrap" marginTop="40">
          <Loader />
        </Container>
      ) : (
        <Container flexDirection="row" alignItems="flex-start">
          <MenuList
            data={animations}
            numColumns={2}
            navigation={navigation}
            to="animationInfo"
          />
        </Container>
      )
    }
    </View>
  )
}