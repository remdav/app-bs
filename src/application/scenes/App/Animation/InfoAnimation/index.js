import { View, } from "application/components/Types"
import { InfoProduct } from "application/components"
import { Text } from "react-native"
import { get } from "utils/request"
import React from "react"

import { Row } from "./components"

export default function InfoAnimation({navigation}) {
  const [animation, setAnimation] = React.useState("")
  const mealId = navigation.getParam("id")
  

  async function getAnimation() {
      let feature

      try {
          feature = await get(`/animations/${mealId}`)
      } catch (e) {
          return setAnimation("Error")
      }

      setAnimation(feature)
  }

  React.useEffect(() => {
    getAnimation()
  }, [])

  return (
    <View>
      <InfoProduct info={animation} />
      <Row>
        <Text style={{color: "white"}}>{animation.description}</Text>
      </Row>
    </View>
  )
}