import {View, Container, MenuList, TextInput, Loader} from "application/components/Types"
import {get} from "utils/request"
import React from "react"


export default function Dashobard({navigation}) {
    const [categories, setCategories] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const[search,setSearch] = React.useState(null)

    async function getAllCategories() {
        let allCategories

        setLoading(true)
        try {
            allCategories = await get(`/categories`)
        } catch (e) {
            console.log(e)
            return
        }

        setLoading(false)
        setCategories(allCategories['hydra:member'])
    }

    async function searchMeal() {
        if(search === "") {
            setSearch(null)
            return
        }

        let resultMeals

        try {
            resultMeals = await get(`/meals?title=${search}`)
        } catch (e) {
            return
        }

        setCategories(resultMeals['hydra:member'])
    }

    function changeText(data) {
        setSearch(data)
    }

    React.useEffect(() => {
        if(search === null) {
            getAllCategories()
        } else {
            searchMeal()
        }
    }, [search])

    return (
        <View>
            {loading ? (
                <Container flexDirection="row" alignItems="flex-start" flexWrap="wrap" marginTop="40">
                    <Loader />
                </Container>
            ) : (
                <>
                    <TextInput
                        placeholder="Recherche"
                        isHeader={true}
                        onChangeText={changeText}
                    />
                    <Container flexDirection="row" alignItems="flex-start" flexWrap="wrap" marginTop="0">
                        <MenuList
                            data={categories}
                            numColumns={2}
                            navigation={navigation}
                            to={search === null ? "mealByCategorie" : "mealInfo"}
                        />
                    </Container>
                </>
            )
        }
        </View>
    )
}
