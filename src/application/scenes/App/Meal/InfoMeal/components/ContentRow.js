import styled from "@emotion/native"

const ContentRow = styled.View`
  flex: 1;
  flex-direction: row;
  flex-drap: wrap;
`

export default ContentRow
