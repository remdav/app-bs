import styled from "@emotion/native"

const Row = styled.View`
  flex: 1;
  margin-top: 30px;
  margin-bottom: 30px;
`

export default Row