import styled from "@emotion/native"

const Title = styled.Text`
  align-self: center;
  font-weight: bold; 
  font-size: 20px;
  color: ${props => props.theme.yellow}
`

export default Title