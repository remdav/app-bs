import { View, Image } from "application/components/Types"
import { InfoProduct } from "application/components"
import { Text } from "react-native"
import { get } from "utils/request"
import React from "react"

import { Row, Title, ContentRow } from "./components"

export default function InfoMeal({navigation}) {
    const [meal, setMeal] = React.useState("")
    const mealId = navigation.getParam("id")

    async function getMeal() {
        let feature

        try {
            feature = await get(`/meals/${mealId}`)
        } catch (e) {
            return setMeal("Error")
        }

        setMeal(feature)
    }

    React.useEffect(() => {
        getMeal()
    }, [])

    return (
        <View>
            <InfoProduct info={meal} isFlex={true}>
                <ContentRow>
                    <Row>
                        <Title>Description</Title>
                        <Text style={{ marginLeft: 50, marginRight: 50, color: "white", fontSize: 15 }}>{meal.description}</Text>
                    </Row>
                    <Row isRight={true}><Title>{meal.title}</Title></Row>
                </ContentRow>
            </InfoProduct>
        </View>
    )
}