import { View, MenuList, TextInput } from "application/components/Types"
import { get } from "utils/request"
import React from "react"

export default function Meals({ navigation }) {
  const[meals, setMeals] = React.useState([])
  const[search,setSearch] = React.useState("")

  async function getMeals() {
    const categorieId = navigation.getParam("id")
    let featuresMeals

    try {
      featuresMeals = await get(`/categories/${categorieId}/meals?title=${search}`)
    } catch (e) {
      console.log("error", e)
      return
    }

    setMeals(featuresMeals["hydra:member"])
  }

  function changeText(data) {
    setSearch(data)
  }

  React.useEffect(() => {
    getMeals()
  }, [search])

  return (
    <View>
      <TextInput
          placeholder="Recherche"
          isHeader={true}
          onChangeText={changeText}
      />
      {!!meals && (
          <MenuList
            data={meals}
            numColumns={2}
            navigation={navigation}
            to="mealInfo"
          />
      )
      }
    </View >
  )
}