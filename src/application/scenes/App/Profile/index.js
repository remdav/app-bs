import { View, Text, Button, Container } from "application/components/Types";
import * as SecureStore from "expo-secure-store";
import { useSelector } from "react-redux";
import React from "react";

export default function Profile({ navigation }) {
  const infoUser = useSelector((state) => state.infoUser.infoUser.data);

  async function logout() {
    await SecureStore.deleteItemAsync("token_key");
    await SecureStore.deleteItemAsync("key");

    navigation.navigate("connection");
  }

  return (
    <View>
      <Container>
        <Text>Pas externe</Text>
        <Button title="Deconnexion" onPress={logout} />
      </Container>
    </View>
  );
}
