import { View, Container, Button, Image } from "application/components/Types"
import { connectionFacebook, connectionGoogle } from "reduxStore/actions/users"
import { useDispatch } from "react-redux"
import { Logo } from "assets/pictures"
import React from "react"

export default function Connection({ navigation }) {
  const dispatch = useDispatch()

  async function signInWithGoogleAsync() {
    dispatch(connectionGoogle(navigation))
  }

  function signInWithFacebookAsync() {
    dispatch(connectionFacebook(navigation))
  }

  function signInWithLocal() {
    return navigation.navigate("connectionLocal")
  }

  function redirectRegister() {
    return navigation.navigate("register")
  }

  return (
    <View>
      <Container marginTop="100">
        <Image source={Logo} resizeScreen={false} local={true} />
      </Container>
      <Container
        justifyContent="flex-end"
        marginBottom={40}
      >
        <Button
          title="Se connecter avec"
          onPress={signInWithGoogleAsync}
          google={true}
          width="80"
        />
        <Button
          title="Se connecter avec"
          onPress={signInWithFacebookAsync}
          facebook={true}
          width="80"
        />
        <Button
          title="Se connecter"
          onPress={signInWithLocal}
          width="80"
        />
        <Button
          title="S'incrire"
          onPress={redirectRegister}
          width="80"
          variant="outlined"
          textColor="white"
        />
      </Container>
    </View>
  )
}
