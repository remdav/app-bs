import { View, Button, Container } from "application/components/Types"
import { TextInput } from "application/components/FormInput"
import { validationConnectionUser } from "validation/users"
import { connection } from "reduxStore/actions/users"
import { useDispatch } from "react-redux"
import { Formik } from "formik"
import React from "react"

export default function ConnectionLocal({ navigation }) {
  const dispatch = useDispatch()

  function signInLocal(values) {
    dispatch(connection(values, navigation))
  }

  return (
    <View>
      <Container>
        <Formik
          initialValues={{
            username: "",
            password: ""
          }}
          validationSchema={validationConnectionUser}
          onSubmit={signInLocal}
        >
          {formikProps => (
            <>
              <TextInput
                placeholder="Nom"
                name="username"
                formikProps={formikProps}
                autoCapitalize="none"
              />
              <TextInput
                placeholder="Mot de passe"
                name="password"
                formikProps={formikProps}
                secureTextEntry={true}
                autoCapitalize="none"
              />
              <Button
                title="Connexion"
                onPress={formikProps.handleSubmit}
              />
            </>
          )}
        </Formik>
      </Container>
    </View>
  )
}
