import { TextInput, ErrorMessage } from "application/components/FormInput"
import { View, Button, Container } from "application/components/Types"
import { validationCreateUser } from "validation/users"
import { useSelector, useDispatch } from "react-redux"
import { register } from "reduxStore/actions/users"
import { translateError } from "utils"
import { Formik } from "formik"
import React from "react"


export default function Register({ navigation }) {
  const infoUser = useSelector(state => state.infoUser.infoUser)
  const [isTouched, setisTouched] = React.useState(false)
  const dispatch = useDispatch()

  function disableTouched() {
    setisTouched(false)
  }

  return (
    <Formik
      initialValues={{
        name: "",
        password: "",
        username: "",
        firstname: "",
        addressMail: "",
        zipCode: "",
        address: "",
        phoneNumber: "",
      }}
      validationSchema={validationCreateUser}
      onSubmit={async values => {
        dispatch(register(values, navigation))

        setisTouched(true)
        return
      }}
    >
      {formikProps => (
        <View>
          <Container>
            <TextInput
              placeholder="Nom d'utilisateur"
              name="username"
              formikProps={formikProps}
              onBlur={disableTouched}
              autoCapitalize="none"
            />
            {!!infoUser.error && isTouched && (
              <ErrorMessage message={translateError(infoUser.data)} />
            )}
            <TextInput
              placeholder="Nom"
              name="name"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Prénom"
              name="firstname"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Numéro de téléphone"
              keyboardType="number-pad"
              name="phoneNumber"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Mot de passe"
              name="password"
              formikProps={formikProps}
              secureTextEntry={true}
              autoCapitalize="none"
            />
            <TextInput
              placeholder="Adresse e-mail"
              name="addressMail"
              keyboardType="email-address"
              formikProps={formikProps}
              autoCapitalize="none"
            />
            <TextInput
              placeholder="Adresse postal"
              name="address"
              formikProps={formikProps}
            />
            <TextInput
              placeholder="Code Postal"
              name="zipCode"
              keyboardType="number-pad"
              formikProps={formikProps}
            />
            <Button
              title="S'inscrire"
              onPress={formikProps.handleSubmit}
            />
          </Container>
        </View>
      )}
    </Formik>
  )
}
