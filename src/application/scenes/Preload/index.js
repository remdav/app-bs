import { yetConnected } from "reduxStore/actions/users"
import { View } from "application/components/Types"
import { ActivityIndicator } from "react-native"
import * as SecureStore from "expo-secure-store"
import { useDispatch } from "react-redux"
import React from "react"



export default function Preload({ navigation }) {
  const dispatch = useDispatch()

  async function findInfoUSer() {
    let infoUser = await SecureStore.getItemAsync("token_key")

    if (infoUser !== null) {
      dispatch(yetConnected(navigation))
      
      return
    }

    navigation.navigate("connection")
  }

  React.useEffect(() => {
    findInfoUSer()
  })

  return (
    <>
      <View>
        <ActivityIndicator size="large" />
      </View>
    </>
  );
}
