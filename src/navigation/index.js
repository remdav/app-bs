import createAnimatedSwitchNavigator from "react-navigation-animated-switch"
import { createAppContainer } from "react-navigation"

import { connection ,app, preload } from "./routes"

const router = createAppContainer(createAnimatedSwitchNavigator(
    {
        preload,
        connection,
        app,
    }
), {
  initialRouteName: "preload",
  backBehavior:"history",
})

export default router