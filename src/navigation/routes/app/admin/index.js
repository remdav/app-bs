import { Admin, Info, Add } from "application/scenes/App/Admin"
import { createStackNavigator } from "react-navigation-stack"
import styled from "config/theme.json"

const adminRouteur = createStackNavigator({
  administration: {
    screen: Admin,
    navigationOptions: {
      headerShown: false,
    },
  },
  infoAdmin: {
    screen: Info,
    navigationOptions: {
      title: "",
      headerStyle: {
          backgroundColor: styled.black,
      },
      headerTintColor: "white",
      headerTitleAlign: "center"
    }
  },
  add: {
    screen: Add,
    navigationOptions: {
      title: "",
      headerStyle: {
          backgroundColor: styled.black,
      },
      headerTintColor: "white",
      headerTitleAlign: "center"
    }
  }
}, {
    defaultNavigationOptions: {
        gestureEnabled: true,
        gestureDirection: "horizontal"
    },
})

export default adminRouteur