import { Animations, InfoAnimation } from "application/scenes/App/Animation"
import { createStackNavigator } from "react-navigation-stack"
import styled from "config/theme.json"

const animationRouteur = createStackNavigator({
  animation: {
    screen: Animations,
    navigationOptions: {
      headerShown: false,
    },
  },
  animationInfo: {
    screen: InfoAnimation,
    navigationOptions: {
      title: "",
      headerStyle: {
          backgroundColor: styled.black,
      },
      headerTintColor: "white",
      headerTitleAlign: "center"
    }
  }

}, {
    defaultNavigationOptions: {
        gestureEnabled: true,
        gestureDirection: "horizontal"
    },
})

export default animationRouteur