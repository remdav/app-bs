import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs"
import MCI from "react-native-vector-icons/MaterialCommunityIcons"
import { Other, Profile } from "application/scenes"
import theme from "config/theme.json"
import React from "react"

import Administration from "./admin"
import Animation from "./animation"
import Meal from "./meal"

export default createMaterialBottomTabNavigator(
  {
    meals: { 
      screen: Meal,
      navigationOptions: {
        tabBarLabel:"Menu",
        tabBarIcon: () => (
          <MCI name="food-fork-drink" size={25}/>
        )
      },
    },
    historical: { 
      screen: Other,
      navigationOptions: {
        tabBarLabel:"Historique",
        tabBarIcon: () => (
          <MCI name="history" size={25}/>
        )
      },
     },
    animation: { 
      screen: Animation,
      navigationOptions: {
        tabBarLabel:"Animation",
        tabBarIcon: () => (
          <MCI name="calendar-clock" size={25}/>
        )
      },
     },
    profile: { 
      screen: Profile,
      navigationOptions: {
        tabBarLabel:"Profil",
        tabBarIcon: () => (
          <MCI name="face-profile" size={25}/>
        )
      },
     },
    administration: { 
      screen: Administration,
      navigationOptions: {
        tabBarLabel:"Administration",
        tabBarIcon: () => (
          <MCI name="lock" size={25}/>
        )
      },
     },
  },
  {
    activeColor: theme.white,
    inactiveColor: theme.black,
    barStyle: { backgroundColor: theme.yellow },
    shifting: false,
  }
)
