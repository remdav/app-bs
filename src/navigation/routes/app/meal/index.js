import { Categories, Meals, InfoMeal } from "application/scenes/App/Meal"
import { createStackNavigator } from "react-navigation-stack"
import styled from "config/theme.json"

const routeMeal = createStackNavigator({
    meals: {
        screen: Categories,
        navigationOptions: {
            headerShown: false,
        }
    },
    mealByCategorie: {
        screen: Meals,
        navigationOptions: {
            title: "",
            headerStyle: {
                backgroundColor: styled.black,
            },
            headerTintColor: "white",
            headerTitleAlign: "center"
        }
    },
    mealInfo: {
        screen: InfoMeal,
        navigationOptions: {
            title: "",
            headerStyle: {
                backgroundColor: styled.black,
            },
            headerTintColor: "white",
            headerTitleAlign: "center"
        }
    },
}, {
    defaultNavigationOptions: {
        gestureEnabled: true,
        gestureDirection: "horizontal"
    },
})

export default routeMeal