import { Connection, Register, ConnectionLocal } from "application/scenes/Connection"
import { createStackNavigator } from "react-navigation-stack"
import styled from "config/theme.json"

const routeConnection = createStackNavigator({
    connection: {
        screen: Connection,
        path: "/connection",
        navigationOptions: {
            headerShown: false,
        }
    },
    connectionLocal: {
        screen: ConnectionLocal,
        path: "/connection/local",
        navigationOptions: {
            title: "Connexion",
            headerStyle: {
                backgroundColor: styled.black,
            },
            headerTintColor: "white",
            headerTitleAlign: "center"
        }
    },
    register: {
        screen: Register,
        path: "/connection/register",
        navigationOptions: {
            title: "Inscription",
            headerStyle: {
                backgroundColor: styled.black,
            },
            headerTintColor: "white",
            headerTitleAlign: "center"
        }
    },
}, {
    defaultNavigationOptions: {
        gestureEnabled: true,
        gestureDirection: "horizontal"
    },
})

export default routeConnection