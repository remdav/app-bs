import { createStackNavigator } from "react-navigation-stack"
import { Preload } from "application/scenes"

const routePreload = createStackNavigator({
    preload: {
        screen: Preload,
        navigationOptions: {
          title: ""
      }
    }
}, {
  headerMode: "none",
})

export default routePreload