import * as types from "reduxStore/constants/users/connection"
import * as SecureStore from "expo-secure-store"
import config from "config/config.json"
import axios from "axios"

export const connectionUser = () => ({
  type: types.CONNECTION_USER,
  loading: true
})

export const connectionUserSuccess = (data) => ({
  type: types.CONNECTION_USER_SUCCESS,
  loading: false,
  error: false,
  data
})

export const connectionUserError = (error) => ({
  type: types.CONNECTION_USER_ERROR,
  loading: false,
  error
})

export default function connection(data, navigation) {
  return async dispatch => {
    dispatch(connectionUser())

    let newUser

    try {
      newUser = await axios.post(`${config.global.server}/login_check`, data)
    } catch (e) {
      console.log("enter", e)
      dispatch(connectionUserError(e))

      return
    }

    dispatch(connectionUserSuccess(newUser.data.items))

    try {
      await Promise.all([
        SecureStore.setItemAsync("token_key", newUser.data.token),
        SecureStore.setItemAsync("refresh_token", newUser.data.refresh_token)
      ])
    } catch (e) {
      console.log(e)
    }

    navigation.navigate("meals")
  }
} 
