import * as types from "reduxStore/constants/users/connection"
import * as SecureStore from "expo-secure-store"
import * as Facebook from "expo-facebook"
import config from "config/config.json"
import { jwtDecode } from "utils"
import axios from "axios"

export const connectionFacebookUser = () => ({
  type: types.CONNECTION_FACEBOOK,
  loading: true,
  error: false
})

export const connectionFacebookUserSuccess = (data) => ({
  type: types.CONNECTION_FACEBOOK_SUCCESS,
  loading: false,
  error: false,
  data
})

export const connectionFacebookUserError = (data) => ({
  type: types.CONNECTION_FACEBOOK_ERROR,
  loading: false,
  error: true,
  data
})


export default function connectionFacebook(navigation) {
  return async dispatch => {
    dispatch(connectionFacebookUser())

    try {
      await Facebook.initializeAsync(config.global.facebookClientId)

      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile'],
      })

      

      if (type === 'success') {
        const response = await axios.get(`https://graph.facebook.com/me?access_token=${token}`)
        let token_key
        

        try { 
          token_key = await axios.post(`${config.global.server}/external_users`, { idExternal: response.data.id, username: response.data.name } )

        } catch (e) {
          dispatch(connectionFacebookUserError(e))
        }

        const tokenKey = token_key.data.token
        const decode = jwtDecode(tokenKey)

        dispatch(connectionFacebookUserSuccess(decode))

        await SecureStore.setItemAsync("token_key", tokenKey)
        navigation.navigate("meals")
      }
    } catch ({ message }) {
      alert(`Erreur dans le login/mot de passe: ${message}`);
    }
  }
}