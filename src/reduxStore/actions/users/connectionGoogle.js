import * as types from "reduxStore/constants/users/connection";
import * as SecureStore from "expo-secure-store";
import * as Google from "expo-google-app-auth";
import config from "config/config.json";
import { jwtDecode } from "utils";
import axios from "axios";

export const connectionGoogleUser = () => ({
  type: types.CONNECTION_GOOGLE,
  loading: true,
});

export const connectionGoogleSuccess = (data) => ({
  type: types.CONNECTION_GOOGLE_SUCCESS,
  loading: false,
  error: false,
  data,
});

export const connectionGoogleError = (data) => ({
  type: types.CONNECTION_GOOGLE_ERROR,
  loading: false,
  error: data,
});

export default function connectionGoogle(navigation) {
  return async (dispatch) => {
    dispatch(connectionGoogleUser());
    let result;

    try {
      result = await Google.logInAsync({
        androidClientId: config.global.androidClientId,
        androidStandaloneAppClientId: config.global.androidStandaloneAppClientId,
        scopes: ["profile", "email"],
      });
    } catch (e) {
      return { error: true };
    }

    if (result.type === "success") {
      let response
      try {
        response = await axios.post(`${config.global.server}/external_users`,{ idExternal: result.user.id, username: result.user.name });
      } catch (e) {
        console.log(e)
        return;
      }
      
      const tokenKey = response.data.token;

      dispatch(connectionGoogleSuccess("ok"));

      await SecureStore.setItemAsync("token_key", tokenKey);

      navigation.navigate("meals");

    } else {
      return { cancelled: true };
    }
  };
}
