import * as types from "reduxStore/constants/users/connection"
import * as errors from "reduxStore/constants/errors"
import * as SecureStore from "expo-secure-store"
import config from "config/config.json"
import { jwtDecode } from "utils"
import axios from "axios"


export const registerUser = () => ({
  type: types.REGISTER_USER,
  loading: true,
  error: false
})

export const registerSuccess = (data) => ({
  type: types.REGISTER_USER_SUCCESS,
  loading: false,
  error: false,
  data
})

export const registerError = (data) => ({
  type: types.REGISTER_USER_ERROR,
  loading: false,
  error: true,
  data
})

export default function register(values, navigation) {
  return async dispatch => {
    dispatch(registerUser())

    let newUser

    try {
      newUser = await axios.post(`${config.global.server}/users`, values)
    } catch (e) {
      console.log("######", e)
      return
    }

    const tokenKey = newUser.data.token
    const decode = jwtDecode(tokenKey)
    
    if (!!tokenKey) {
      console.log("rfsde")
      try {
        await SecureStore.setItemAsync("token_key", tokenKey)
      } catch (e) {
        return
      }

      dispatch(registerSuccess(decode))
      navigation.navigate("meals")
      return
    }


    dispatch(registerError(errors.EXIST_USER))
  }
}