import * as types from "reduxStore/constants/users/connection"
import * as SecureStore from "expo-secure-store"
import { jwtDecode } from "utils"


export const yetConnectedUser = () => ({
  type: types.YET_CONNECTION_USER,
  loading: true,
  error: false
})

export const yetConnectedUserSuccess = (data) => ({
  type: types.YET_CONNECTION_USER_SUCCESS,
  loading: false,
  error: false,
  data
})

export const yetConnectedUserError = (error) => ({
  type: types.YET_CONNECTION_USER_ERROR,
  loading: false,
  error: true,
  data: error
})

export default function yetConnected(navigation) {
  return async dispatch => {
    dispatch(yetConnectedUser())
    let tokenKey

    try {
      tokenKey = await SecureStore.getItemAsync("token_key")
    } catch (e) {
      dispatch(yetConnectedUserError(e))

      return
    }
    
    const decode = jwtDecode(tokenKey)

    dispatch(yetConnectedUserSuccess(decode.info))
  
    navigation.navigate("meals")
  }
}
