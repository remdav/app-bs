import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"

import reducerRoot from "./reducers"

export default createStore(reducerRoot, applyMiddleware(thunk))