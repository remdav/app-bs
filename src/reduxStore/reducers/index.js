import { combineReducers } from "redux"
import connection from "./users/connection"

const reducerRoot = combineReducers({ 
  infoUser: connection,
 })

export default reducerRoot