import * as types from "reduxStore/constants/users/connection"

const initialState = { infoUser: {} }

export default function connection(state = initialState, action) {
  let nextState
  const infoUser = {
    error: action.error,
    loading: action.loading,
    data: action.data
  }

  switch (action.type) {
    case types.CONNECTION_USER_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.CONNECTION_USER_ERROR:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.YET_CONNECTION_USER_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.CONNECTION_FACEBOOK_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.YET_CONNECTION_USER_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.CONNECTION_GOOGLE_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.REGISTER_USER_SUCCESS:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    case types.REGISTER_USER_ERROR:
      nextState = {
        ...state,
        infoUser
      }

      return nextState

    default:
      return state
  }
}
