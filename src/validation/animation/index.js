import * as Yup from "yup"

export const validationAnimation = Yup.object({
  title: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  theme: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  description: Yup.string().min(2, "Minimim 40 caractères").required("* Champ obligatoire"),
  speaker: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  startAt: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  endAt: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  image: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
})