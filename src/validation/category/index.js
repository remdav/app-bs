import * as Yup from "yup"

export const validationCategory = Yup.object({
  title: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  image: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
})