import * as Yup from "yup"

export const validationMeal = Yup.object({
  category: Yup.number().required("* Champ obligatoire"),
  title: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  description: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  stock: Yup.string().required("* Champ obligatoire"),
  price: Yup.string().required("* Champ obligatoire"),
  image: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
})