import * as Yup from "yup"

export const validationConnectionUser = Yup.object({
  username: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  password: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
})

export const validationCreateUser = Yup.object({
  name: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  password: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  username: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  firstname: Yup.string().min(2, "Minimim 2 caractères").required("* Champ obligatoire"),
  addressMail: Yup.string().email("Adresse e-mail non valide").required("* Champ obligatoire"),
  zipCode: Yup.number().integer().min(5, "Minimim 5 chiffres").required("* Champ obligatoire"),
  phoneNumber: Yup.number().integer().min(10, "Minimim 10 chiffres").required("* Champ obligatoire"),
  address: Yup.string().min(20, "Minimim 20 caractères").required("* Champ obligatoire"),
})
