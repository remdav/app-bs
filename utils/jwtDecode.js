import { Buffer } from 'buffer'

export default function jwtDecode(token) {
  const decode = token.split('.').map(part => new Buffer.from(part.replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString())
  return JSON.parse(decode[1]);
}