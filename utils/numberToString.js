export default function numberToString(number) {
  if(typeof number === "number") {
    return number.toString()
  }
}