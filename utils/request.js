import * as SecureStore from "expo-secure-store"
import config from "config/config.json"
import axios from "axios"

function bearer(token) {
  return { Authorization: `Bearer ${token}` } 
}

async function request(method, url, data) {
  const refresh_token = await SecureStore.getItemAsync("refresh_token")
  const token = await SecureStore.getItemAsync("token_key")
  let opts = { method }
  
  opts.headers = bearer(token)
  opts.url = `${config.global.server}${url}`

  if(data) {
    if(method === "get") {
      opts.params = data
    } else {
      opts.data = data
    }
  }

  let execute

  try {
    execute = await axios(opts)
  } catch (error) {
    if(error.response.status === 401) {
      await SecureStore.deleteItemAsync("token_key")

      let newToken

      try {
        newToken = await axios.post(`${config.global.server}/token/refresh`, { refresh_token })
      } catch (e) {
        console.log("####", e)
      }

      await SecureStore.setItemAsync("token_key", newToken.data.token)
      
      const OldConfig = error.config
      OldConfig.headers['Authorization'] = `Bearer ${newToken.data.token}`
      let reExecuteRequest

      try {
        reExecuteRequest = await axios.request(OldConfig)
      } catch (e) {
        console.log(e)
      }

      return reExecuteRequest.data  
    } else {
      console.log("Request error", error)
    }
  }

  return execute.data
}

export function get(url, data) {
  return request("get", url, data)
}

export function post(url, data) {
  return request("post", url, data)
}

export function patch(url, data) {
  return request("patch", url, data)
}

export function deletes(url, data) {
  return request("patch", url, data)
}