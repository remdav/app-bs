import { Dimensions } from "react-native"

import numberToString from "./numberToString"

export default function sizeScreen(divisedBy = 2.5) {
  const  { width } = Dimensions.get('window')
  return numberToString(width / divisedBy)
}
