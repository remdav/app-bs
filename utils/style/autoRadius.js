export default function autoRadius(isCutUp) {
  if(isCutUp) {
    return "borderRadius: 0px 0px 30px 30px;"
  }

  return "borderRadius: 40px;"
}